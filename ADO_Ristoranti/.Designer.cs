﻿namespace ADO_Ristoranti
{
    partial class frmVisualizzaModifica
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvRistoranti = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.baseDiDatiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.creaLeTabelleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ristorantiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aggiungiUnRistoranteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.carteDiCreditoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aggiungiCartaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbxRistoranti = new System.Windows.Forms.GroupBox();
            this.btnEliminaRistorante = new System.Windows.Forms.Button();
            this.btnDisaccoppia = new System.Windows.Forms.Button();
            this.lstCarteAss = new System.Windows.Forms.ListBox();
            this.lblCartaAss = new System.Windows.Forms.Label();
            this.gbxCarteCredito = new System.Windows.Forms.GroupBox();
            this.btnEliminaCarta = new System.Windows.Forms.Button();
            this.btnAccoppia = new System.Windows.Forms.Button();
            this.dgvCarteCredito = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRistoranti)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.gbxRistoranti.SuspendLayout();
            this.gbxCarteCredito.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCarteCredito)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvRistoranti
            // 
            this.dgvRistoranti.AllowUserToAddRows = false;
            this.dgvRistoranti.AllowUserToDeleteRows = false;
            this.dgvRistoranti.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRistoranti.Location = new System.Drawing.Point(6, 19);
            this.dgvRistoranti.MultiSelect = false;
            this.dgvRistoranti.Name = "dgvRistoranti";
            this.dgvRistoranti.Size = new System.Drawing.Size(669, 205);
            this.dgvRistoranti.TabIndex = 0;
            this.dgvRistoranti.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRistoranti_CellValueChanged);
            this.dgvRistoranti.Click += new System.EventHandler(this.dgvRistoranti_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.baseDiDatiToolStripMenuItem,
            this.ristorantiToolStripMenuItem,
            this.carteDiCreditoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(951, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // baseDiDatiToolStripMenuItem
            // 
            this.baseDiDatiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.creaLeTabelleToolStripMenuItem});
            this.baseDiDatiToolStripMenuItem.Name = "baseDiDatiToolStripMenuItem";
            this.baseDiDatiToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.baseDiDatiToolStripMenuItem.Text = "Base di dati";
            // 
            // creaLeTabelleToolStripMenuItem
            // 
            this.creaLeTabelleToolStripMenuItem.Name = "creaLeTabelleToolStripMenuItem";
            this.creaLeTabelleToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.creaLeTabelleToolStripMenuItem.Text = "Crea le tabelle";
            this.creaLeTabelleToolStripMenuItem.Click += new System.EventHandler(this.creaLeTabelleToolStripMenuItem_Click);
            // 
            // ristorantiToolStripMenuItem
            // 
            this.ristorantiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aggiungiUnRistoranteToolStripMenuItem});
            this.ristorantiToolStripMenuItem.Enabled = false;
            this.ristorantiToolStripMenuItem.Name = "ristorantiToolStripMenuItem";
            this.ristorantiToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.ristorantiToolStripMenuItem.Text = "Ristoranti";
            // 
            // aggiungiUnRistoranteToolStripMenuItem
            // 
            this.aggiungiUnRistoranteToolStripMenuItem.Name = "aggiungiUnRistoranteToolStripMenuItem";
            this.aggiungiUnRistoranteToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.aggiungiUnRistoranteToolStripMenuItem.Text = "Aggiungi un ristorante";
            this.aggiungiUnRistoranteToolStripMenuItem.Click += new System.EventHandler(this.aggiungiUnRistoranteToolStripMenuItem_Click);
            // 
            // carteDiCreditoToolStripMenuItem
            // 
            this.carteDiCreditoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aggiungiCartaToolStripMenuItem});
            this.carteDiCreditoToolStripMenuItem.Enabled = false;
            this.carteDiCreditoToolStripMenuItem.Name = "carteDiCreditoToolStripMenuItem";
            this.carteDiCreditoToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.carteDiCreditoToolStripMenuItem.Text = "Carte di credito";
            // 
            // aggiungiCartaToolStripMenuItem
            // 
            this.aggiungiCartaToolStripMenuItem.Name = "aggiungiCartaToolStripMenuItem";
            this.aggiungiCartaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.aggiungiCartaToolStripMenuItem.Text = "Aggiungi carta";
            this.aggiungiCartaToolStripMenuItem.Click += new System.EventHandler(this.aggiungiCartaToolStripMenuItem_Click);
            // 
            // gbxRistoranti
            // 
            this.gbxRistoranti.Controls.Add(this.btnEliminaRistorante);
            this.gbxRistoranti.Controls.Add(this.btnDisaccoppia);
            this.gbxRistoranti.Controls.Add(this.lstCarteAss);
            this.gbxRistoranti.Controls.Add(this.lblCartaAss);
            this.gbxRistoranti.Controls.Add(this.dgvRistoranti);
            this.gbxRistoranti.Enabled = false;
            this.gbxRistoranti.Location = new System.Drawing.Point(12, 27);
            this.gbxRistoranti.Name = "gbxRistoranti";
            this.gbxRistoranti.Size = new System.Drawing.Size(927, 259);
            this.gbxRistoranti.TabIndex = 7;
            this.gbxRistoranti.TabStop = false;
            this.gbxRistoranti.Text = "Ristoranti";
            // 
            // btnEliminaRistorante
            // 
            this.btnEliminaRistorante.Location = new System.Drawing.Point(6, 230);
            this.btnEliminaRistorante.Name = "btnEliminaRistorante";
            this.btnEliminaRistorante.Size = new System.Drawing.Size(669, 23);
            this.btnEliminaRistorante.TabIndex = 9;
            this.btnEliminaRistorante.Text = "Elimina il ristorante selezionato";
            this.btnEliminaRistorante.UseVisualStyleBackColor = true;
            this.btnEliminaRistorante.Click += new System.EventHandler(this.btnEliminaRistorante_Click);
            // 
            // btnDisaccoppia
            // 
            this.btnDisaccoppia.Enabled = false;
            this.btnDisaccoppia.Location = new System.Drawing.Point(703, 230);
            this.btnDisaccoppia.Name = "btnDisaccoppia";
            this.btnDisaccoppia.Size = new System.Drawing.Size(218, 23);
            this.btnDisaccoppia.TabIndex = 9;
            this.btnDisaccoppia.Text = "Disaccoppia";
            this.btnDisaccoppia.UseVisualStyleBackColor = true;
            this.btnDisaccoppia.Click += new System.EventHandler(this.btnDisaccoppia_Click);
            // 
            // lstCarteAss
            // 
            this.lstCarteAss.FormattingEnabled = true;
            this.lstCarteAss.Location = new System.Drawing.Point(703, 35);
            this.lstCarteAss.Name = "lstCarteAss";
            this.lstCarteAss.Size = new System.Drawing.Size(218, 186);
            this.lstCarteAss.TabIndex = 8;
            this.lstCarteAss.SelectedIndexChanged += new System.EventHandler(this.lstCarteAss_SelectedIndexChanged);
            // 
            // lblCartaAss
            // 
            this.lblCartaAss.AutoSize = true;
            this.lblCartaAss.Location = new System.Drawing.Point(700, 19);
            this.lblCartaAss.Name = "lblCartaAss";
            this.lblCartaAss.Size = new System.Drawing.Size(80, 13);
            this.lblCartaAss.TabIndex = 7;
            this.lblCartaAss.Text = "Carta associata";
            // 
            // gbxCarteCredito
            // 
            this.gbxCarteCredito.Controls.Add(this.btnEliminaCarta);
            this.gbxCarteCredito.Controls.Add(this.btnAccoppia);
            this.gbxCarteCredito.Controls.Add(this.dgvCarteCredito);
            this.gbxCarteCredito.Enabled = false;
            this.gbxCarteCredito.Location = new System.Drawing.Point(12, 292);
            this.gbxCarteCredito.Name = "gbxCarteCredito";
            this.gbxCarteCredito.Size = new System.Drawing.Size(927, 192);
            this.gbxCarteCredito.TabIndex = 8;
            this.gbxCarteCredito.TabStop = false;
            this.gbxCarteCredito.Text = "Carte di credito";
            // 
            // btnEliminaCarta
            // 
            this.btnEliminaCarta.Location = new System.Drawing.Point(6, 164);
            this.btnEliminaCarta.Name = "btnEliminaCarta";
            this.btnEliminaCarta.Size = new System.Drawing.Size(568, 23);
            this.btnEliminaCarta.TabIndex = 10;
            this.btnEliminaCarta.Text = "Elimina la carta selezionata";
            this.btnEliminaCarta.UseVisualStyleBackColor = true;
            this.btnEliminaCarta.Click += new System.EventHandler(this.btnEliminaCarta_Click);
            // 
            // btnAccoppia
            // 
            this.btnAccoppia.Location = new System.Drawing.Point(600, 85);
            this.btnAccoppia.Name = "btnAccoppia";
            this.btnAccoppia.Size = new System.Drawing.Size(305, 50);
            this.btnAccoppia.TabIndex = 10;
            this.btnAccoppia.Text = "Accoppia al ristorante selezionato";
            this.btnAccoppia.UseVisualStyleBackColor = true;
            this.btnAccoppia.Click += new System.EventHandler(this.btnAccoppia_Click);
            // 
            // dgvCarteCredito
            // 
            this.dgvCarteCredito.AllowUserToAddRows = false;
            this.dgvCarteCredito.AllowUserToDeleteRows = false;
            this.dgvCarteCredito.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCarteCredito.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCarteCredito.Location = new System.Drawing.Point(6, 19);
            this.dgvCarteCredito.MultiSelect = false;
            this.dgvCarteCredito.Name = "dgvCarteCredito";
            this.dgvCarteCredito.Size = new System.Drawing.Size(568, 139);
            this.dgvCarteCredito.TabIndex = 0;
            this.dgvCarteCredito.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCarteCredito_CellValueChanged);
            // 
            // frmVisualizzaModifica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(951, 513);
            this.Controls.Add(this.gbxCarteCredito);
            this.Controls.Add(this.gbxRistoranti);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmVisualizzaModifica";
            this.Text = "Ristoranti";
            this.Activated += new System.EventHandler(this.frmVisualizzaModifica_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRistoranti)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.gbxRistoranti.ResumeLayout(false);
            this.gbxRistoranti.PerformLayout();
            this.gbxCarteCredito.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCarteCredito)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvRistoranti;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ristorantiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aggiungiUnRistoranteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem carteDiCreditoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aggiungiCartaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem baseDiDatiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem creaLeTabelleToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbxRistoranti;
        private System.Windows.Forms.Button btnDisaccoppia;
        private System.Windows.Forms.ListBox lstCarteAss;
        private System.Windows.Forms.GroupBox gbxCarteCredito;
        private System.Windows.Forms.Button btnAccoppia;
        private System.Windows.Forms.DataGridView dgvCarteCredito;
        private System.Windows.Forms.Button btnEliminaRistorante;
        private System.Windows.Forms.Button btnEliminaCarta;
        private System.Windows.Forms.Label lblCartaAss;
    }
}

