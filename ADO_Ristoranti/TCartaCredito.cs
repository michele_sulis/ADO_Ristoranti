﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO_Ristoranti
{
    static class TCartaCredito
    {
        public static OleDbConnection cn = new OleDbConnection(Program.cnStr);
        public static string CreateTable()
        {
            string err = "";

            try
            {
                string SQLcmd = "CREATE TABLE CartaCredito (ID_identificativo AUTOINCREMENT PRIMARY KEY, Nome varchar(20) NOT NULL, Num_verde varchar(10) NOT NULL);";

                cn.Open();
                OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
                cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                err = e.Message + "\n";
                if (e.Message != "Table 'CartaCredito' already exists.")
                    throw e;
            }

            return err;
        }

        public static void AddCreditCard(CartaCredito c)
        {
            string SQLcmd = "INSERT INTO CartaCredito (Nome, Num_verde) VALUES (@Nome, @Num_verde);";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
            cmd.Parameters.AddWithValue("@Nome", c.nome);
            cmd.Parameters.AddWithValue("@Num_verde", c.num_verde);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void SameCards(CartaCredito c)
        {
            string SQLcmd = "SELECT Nome FROM CartaCredito WHERE Nome = @Nome AND Num_verde = @Num_verde";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
            cmd.Parameters.AddWithValue("@Nome", c.nome);
            cmd.Parameters.AddWithValue("@Num_verde", c.num_verde);
            string NomeCarta = (string)cmd.ExecuteScalar();
            cn.Close();

            if (NomeCarta != null)
                throw new Exception("Esiste già la carta di credito '" + NomeCarta + "' con gli stessi dati!");
        }

        public static int RowCount()
        {
            string cmdString = "SELECT COUNT (*) FROM CartaCredito";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandText = cmdString;
            int n = (int)cmd.ExecuteScalar();
            cn.Close();
            return n;
        }

        public static bool Empty { get { return RowCount() == 0; } }

        public static bool ExistingTable()
        {
            try
            {
                RowCount();
            }
            catch (Exception ex)
            {
                if (ex.Message == "The Microsoft Office Access database engine cannot find the input table or query 'CartaCredito'.  Make sure it exists and that its name is spelled correctly.")
                    return false;
            }
            return true;
        }

        public static DataSet GetCreditCards()
        {
            string SQLcmd = "SELECT * FROM CartaCredito";

            cn.Open();
            DataSet ds = new DataSet();
            OleDbDataAdapter da = new OleDbDataAdapter(SQLcmd, cn);
            da.Fill(ds, "CarteCredito");
            cn.Close();
            return ds;
        }

        public static void UpdateCreditCard(CartaCredito c)
        {
            string SQLcmd = "UPDATE CartaCredito SET Nome = @Nome, Num_verde = @Num_verde WHERE ID_identificativo = @ID_identificativo;";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
            cmd.Parameters.AddWithValue("@Nome", c.nome);
            cmd.Parameters.AddWithValue("@Num_verde", c.num_verde);
            cmd.Parameters.AddWithValue("@ID_identificativo", c.ID_identificativo);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void DeleteCreditCard(int ID_identificativo)
        {
            string SQLcmd = "DELETE FROM CartaCredito WHERE ID_identificativo = @ID_identificativo;";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
            cmd.Parameters.AddWithValue("@ID_identificativo", ID_identificativo);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static List<string> NameOfCreditCardByIDs(List<int> ID_carta)
        {
            List<string> Nomi = new List<string>();
            string SQLcmd = "SELECT Nome FROM CartaCredito WHERE ID_identificativo = @ID_identificativo;";

            cn.Open();
            foreach (int id in ID_carta)
            {
                OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
                cmd.Parameters.AddWithValue("@ID_identificativo", id);
                Nomi.Add((string)cmd.ExecuteScalar());
            }

            cn.Close();
            return Nomi;
        }
    }
}
