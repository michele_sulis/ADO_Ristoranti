﻿namespace ADO_Ristoranti
{
    partial class frmAggiungiRistorante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtIndirizzo = new System.Windows.Forms.TextBox();
            this.txtProvincia = new System.Windows.Forms.TextBox();
            this.nudNCivico = new System.Windows.Forms.NumericUpDown();
            this.btnAggiungi = new System.Windows.Forms.Button();
            this.nudStelleMichelin = new System.Windows.Forms.NumericUpDown();
            this.lblCitta = new System.Windows.Forms.Label();
            this.txtCitta = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudNCivico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStelleMichelin)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Indirizzo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(268, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "N°";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(236, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Provincia";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Stelle Michelin";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(93, 16);
            this.txtNome.MaxLength = 30;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(261, 20);
            this.txtNome.TabIndex = 6;
            this.txtNome.TextChanged += new System.EventHandler(this.txtNome_TextChanged);
            // 
            // txtIndirizzo
            // 
            this.txtIndirizzo.Location = new System.Drawing.Point(93, 41);
            this.txtIndirizzo.MaxLength = 30;
            this.txtIndirizzo.Name = "txtIndirizzo";
            this.txtIndirizzo.Size = new System.Drawing.Size(169, 20);
            this.txtIndirizzo.TabIndex = 7;
            this.txtIndirizzo.TextChanged += new System.EventHandler(this.txtIndirizzo_TextChanged);
            // 
            // txtProvincia
            // 
            this.txtProvincia.Location = new System.Drawing.Point(293, 68);
            this.txtProvincia.MaxLength = 2;
            this.txtProvincia.Name = "txtProvincia";
            this.txtProvincia.Size = new System.Drawing.Size(61, 20);
            this.txtProvincia.TabIndex = 9;
            this.txtProvincia.TextChanged += new System.EventHandler(this.txtProvincia_TextChanged);
            // 
            // nudNCivico
            // 
            this.nudNCivico.ImeMode = System.Windows.Forms.ImeMode.On;
            this.nudNCivico.Location = new System.Drawing.Point(293, 42);
            this.nudNCivico.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.nudNCivico.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudNCivico.Name = "nudNCivico";
            this.nudNCivico.ReadOnly = true;
            this.nudNCivico.Size = new System.Drawing.Size(61, 20);
            this.nudNCivico.TabIndex = 11;
            this.nudNCivico.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnAggiungi
            // 
            this.btnAggiungi.Enabled = false;
            this.btnAggiungi.Location = new System.Drawing.Point(15, 134);
            this.btnAggiungi.Name = "btnAggiungi";
            this.btnAggiungi.Size = new System.Drawing.Size(339, 23);
            this.btnAggiungi.TabIndex = 12;
            this.btnAggiungi.Text = "Inserisci";
            this.btnAggiungi.UseVisualStyleBackColor = true;
            this.btnAggiungi.Click += new System.EventHandler(this.btnAggiungi_Click);
            // 
            // nudStelleMichelin
            // 
            this.nudStelleMichelin.Location = new System.Drawing.Point(93, 94);
            this.nudStelleMichelin.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudStelleMichelin.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudStelleMichelin.Name = "nudStelleMichelin";
            this.nudStelleMichelin.ReadOnly = true;
            this.nudStelleMichelin.Size = new System.Drawing.Size(261, 20);
            this.nudStelleMichelin.TabIndex = 13;
            this.nudStelleMichelin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblCitta
            // 
            this.lblCitta.AutoSize = true;
            this.lblCitta.Location = new System.Drawing.Point(12, 71);
            this.lblCitta.Name = "lblCitta";
            this.lblCitta.Size = new System.Drawing.Size(28, 13);
            this.lblCitta.TabIndex = 14;
            this.lblCitta.Text = "Città";
            // 
            // txtCitta
            // 
            this.txtCitta.Location = new System.Drawing.Point(93, 68);
            this.txtCitta.MaxLength = 20;
            this.txtCitta.Name = "txtCitta";
            this.txtCitta.Size = new System.Drawing.Size(137, 20);
            this.txtCitta.TabIndex = 15;
            this.txtCitta.TextChanged += new System.EventHandler(this.txtCitta_TextChanged);
            // 
            // frmAggiungiRistorante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 169);
            this.Controls.Add(this.txtCitta);
            this.Controls.Add(this.lblCitta);
            this.Controls.Add(this.nudStelleMichelin);
            this.Controls.Add(this.btnAggiungi);
            this.Controls.Add(this.nudNCivico);
            this.Controls.Add(this.txtProvincia);
            this.Controls.Add(this.txtIndirizzo);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "frmAggiungiRistorante";
            this.Text = "Ristorante";
            ((System.ComponentModel.ISupportInitialize)(this.nudNCivico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStelleMichelin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtIndirizzo;
        private System.Windows.Forms.TextBox txtProvincia;
        private System.Windows.Forms.NumericUpDown nudNCivico;
        private System.Windows.Forms.Button btnAggiungi;
        private System.Windows.Forms.NumericUpDown nudStelleMichelin;
        private System.Windows.Forms.Label lblCitta;
        private System.Windows.Forms.TextBox txtCitta;
    }
}