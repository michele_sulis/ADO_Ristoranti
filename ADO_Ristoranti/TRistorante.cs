﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO_Ristoranti
{
    static class TRistorante
    {
        public static OleDbConnection cn = new OleDbConnection(Program.cnStr);

        public static string CreateTable()
        {
            string err = "";

            try
            {
                string SQLcmd = "CREATE TABLE Ristorante (ID_cod  AUTOINCREMENT PRIMARY KEY, Nome varchar(30) NOT NULL, Indirizzo varchar(30) NOT NULL," +
                    " N_civico int NOT NULL, Citta varchar(20) NOT NULL, Provincia varchar(2), Livello int);";

                cn.Open();
                OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
                cmd.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                err = e.Message + "\n";
                if (e.Message != "Table 'Ristorante' already exists.")
                    throw e;
            }
            finally
            {
                cn.Close();
            }

            return err;
        }

        public static void AddRestaurant(Ristorante r)
        {
            string SQLcmd = "INSERT INTO Ristorante (Nome, Indirizzo, N_civico, Citta, Provincia, Livello) VALUES (@Nome, @Indirizzo, @N_civico, @Citta, @PR, @Livello);";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
            cmd.Parameters.AddWithValue("@Nome", r.nome);
            cmd.Parameters.AddWithValue("@Indirizzo", r.indirizzo);
            cmd.Parameters.AddWithValue("@N_civico", r.n_civico);
            cmd.Parameters.AddWithValue("@Citta", r.citta);
            cmd.Parameters.AddWithValue("@Pr", r.provincia);
            cmd.Parameters.AddWithValue("@Livello", r.livello);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static DataSet GetRestaurants()
        {
            string SQLcmd = "SELECT * FROM Ristorante";

            cn.Open();
            DataSet ds = new DataSet();
            OleDbDataAdapter da = new OleDbDataAdapter(SQLcmd, cn);
            da.Fill(ds, "Ristoranti");
            cn.Close();
            return ds;
        }

        public static void RestaurantWithSameLocation(Ristorante r)
        {
            string SQLcmd = "SELECT Nome FROM Ristorante WHERE Indirizzo = @Indirizzo AND N_civico = @N_civico AND Citta = @Citta";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
            cmd.Parameters.AddWithValue("@Indirizzo", r.indirizzo);
            cmd.Parameters.AddWithValue("@N_civico", r.n_civico);
            cmd.Parameters.AddWithValue("@Citta", r.citta);
            string NomeRistorante = (string)cmd.ExecuteScalar();
            cn.Close();

            if (NomeRistorante != null)
                throw new Exception("Esiste già il ristorante '" + NomeRistorante + "' con la stessa locazione!");
        }

        public static int RowCount()
        {
            string cmdString = "SELECT COUNT (*) FROM Ristorante";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandText = cmdString;
            int n = (int)cmd.ExecuteScalar();
            cn.Close();
            return n;
        }

        public static bool Empty { get { return RowCount() == 0; } }

        public static bool ExistingTable()
        {
            try
            {
                RowCount();
            }
            catch (Exception ex)
            {
                if (ex.Message == "The Microsoft Office Access database engine cannot find the input table or query 'Ristorante'.  Make sure it exists and that its name is spelled correctly.")
                    return false;
            }
            return true;
        }

        public static void UpdateRestaurant(Ristorante r)
        {
            string SQLcmd = "UPDATE Ristorante SET Nome = @Nome, Indirizzo = @Indirizzo, N_civico = @N_civico, Citta = @Citta, Provincia = @Pr, Livello = @Livello WHERE ID_cod = @ID_cod;";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
            cmd.Parameters.AddWithValue("@Nome", r.nome);
            cmd.Parameters.AddWithValue("@Indirizzo", r.indirizzo);
            cmd.Parameters.AddWithValue("@N_civico", r.n_civico);
            cmd.Parameters.AddWithValue("@Citta", r.citta);
            cmd.Parameters.AddWithValue("@Pr", r.provincia);
            cmd.Parameters.AddWithValue("@Livello", r.livello);
            cmd.Parameters.AddWithValue("@ID_cod", r.ID_cod);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static void DeleteRestaurant(int ID_cod)
        {
            string SQLcmd = "DELETE FROM Ristorante WHERE ID_cod = @ID_cod;";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
            cmd.Parameters.AddWithValue("@ID_cod", ID_cod);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
    }
}
