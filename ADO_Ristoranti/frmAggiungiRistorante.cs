﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ADO_Ristoranti
{
    public partial class frmAggiungiRistorante : Form
    {
        public frmAggiungiRistorante()
        {
            InitializeComponent();
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtIndirizzo.Text) || string.IsNullOrWhiteSpace(txtNome.Text) || string.IsNullOrWhiteSpace(txtProvincia.Text) || string.IsNullOrWhiteSpace(txtCitta.Text))
                btnAggiungi.Enabled = false;
            else
                btnAggiungi.Enabled = true;
        }

        private void txtIndirizzo_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtIndirizzo.Text) || string.IsNullOrWhiteSpace(txtNome.Text) || string.IsNullOrWhiteSpace(txtProvincia.Text) || string.IsNullOrWhiteSpace(txtCitta.Text))
                btnAggiungi.Enabled = false;
            else
                btnAggiungi.Enabled = true;
        }

        private void txtProvincia_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtIndirizzo.Text) || string.IsNullOrWhiteSpace(txtNome.Text) || string.IsNullOrWhiteSpace(txtProvincia.Text) || string.IsNullOrWhiteSpace(txtCitta.Text))
                btnAggiungi.Enabled = false;
            else
                btnAggiungi.Enabled = true;
        }

        private void txtCitta_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtIndirizzo.Text) || string.IsNullOrWhiteSpace(txtNome.Text) || string.IsNullOrWhiteSpace(txtProvincia.Text) || string.IsNullOrWhiteSpace(txtCitta.Text))
                btnAggiungi.Enabled = false;
            else
                btnAggiungi.Enabled = true;
        }

        private void btnAggiungi_Click(object sender, EventArgs e)
        {
            try
            {
                Ristorante r = new Ristorante(txtNome.Text, txtIndirizzo.Text, (int)nudNCivico.Value, txtCitta.Text, txtProvincia.Text, (int)nudStelleMichelin.Value);
                TRistorante.AddRestaurant(r);

                ControlsAsDefault(txtCitta, txtIndirizzo, txtNome, txtProvincia);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void ControlsAsDefault(params TextBox[] tx)
        {
            foreach (var item in tx)
                item.Clear();

            nudNCivico.Value = 1;
            nudStelleMichelin.Value = 1;
        }
    }
}
