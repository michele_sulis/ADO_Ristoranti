﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ADO_Ristoranti
{
    public partial class frmVisualizzaModifica : Form
    {
        public frmVisualizzaModifica()
        {
            InitializeComponent();
        }

        private void aggiungiUnRistoranteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmAggiungiRistorante().ShowDialog();
        }

        private void aggiungiCartaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new frmAggiungiCarta().ShowDialog();
        }

        private void creaLeTabelleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string ErrorMessage = "";
            try
            {
                ErrorMessage += TRistorante.CreateTable();
                ErrorMessage += TCartaCredito.CreateTable();
                ErrorMessage += TAccetta.CreateTable();
            }
            catch (Exception ex)
            {
                ErrorMessage += ex.Message;
            }
            finally
            {
                if (!string.IsNullOrEmpty(ErrorMessage))
                    MessageBox.Show(ErrorMessage, "Esito", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                else
                {
                    MessageBox.Show("Tabelle create correttamente!", "Esito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ristorantiToolStripMenuItem.Enabled = true;
                    carteDiCreditoToolStripMenuItem.Enabled = true;
                }
            }
        }

        private void lstCarteAss_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstCarteAss.SelectedIndex != -1)
                btnDisaccoppia.Enabled = true;
            else
                btnDisaccoppia.Enabled = false;
        }

        private void frmVisualizzaModifica_Activated(object sender, EventArgs e)
        {
            UpdateGbxRistoranti();
            UpdateGbxCarteDiCredito();
        }

        void UpdateGbxRistoranti()
        {
            try
            {
                if (TRistorante.ExistingTable())
                {
                    ristorantiToolStripMenuItem.Enabled = true;
                    if (!TRistorante.Empty)
                    {
                        gbxRistoranti.Enabled = true;
                        dgvRistoranti.DataSource = TRistorante.GetRestaurants();
                        dgvRistoranti.DataMember = "Ristoranti";
                        dgvRistoranti.Columns[0].ReadOnly = true;
                    }
                    else
                        gbxRistoranti.Enabled = false;
                }
                else
                {
                    ristorantiToolStripMenuItem.Enabled = false;
                    gbxRistoranti.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TRistorante.cn.Close();
            }
        }

        void UpdateGbxCarteDiCredito()
        {
            try
            {
                if (TCartaCredito.ExistingTable())
                {
                    carteDiCreditoToolStripMenuItem.Enabled = true;
                    if (!TCartaCredito.Empty)
                    {
                        gbxCarteCredito.Enabled = true;
                        dgvCarteCredito.DataSource = TCartaCredito.GetCreditCards();
                        dgvCarteCredito.DataMember = "CarteCredito";
                        dgvCarteCredito.Columns[0].ReadOnly = true;
                    }
                    else
                        gbxCarteCredito.Enabled = false;
                }
                else
                {
                    carteDiCreditoToolStripMenuItem.Enabled = false;
                    gbxCarteCredito.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TCartaCredito.cn.Close();
            }
        }

        Ristorante RestaurantByDataGridRow(DataGridViewCellCollection dgvc)
        {
            return new Ristorante(Convert.ToInt32(dgvc[0].Value), (string)dgvc[1].Value, (string)dgvc[2].Value, Convert.ToInt32(dgvc[3].Value), (string)dgvc[4].Value, (string)dgvc[5].Value, Convert.ToInt32(dgvc[6].Value));
        }

        CartaCredito CreditCardByDataGridRow(DataGridViewCellCollection dgvc)
        {
            return new CartaCredito(Convert.ToInt32(dgvc[0].Value), (string)dgvc[1].Value, (string)dgvc[2].Value);
        }

        private void btnEliminaRistorante_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dgvr in dgvRistoranti.SelectedRows)
                {
                    TRistorante.DeleteRestaurant(Convert.ToInt32(dgvr.Cells[0].Value));
                    TAccetta.DeleteLinkRestaurant(Convert.ToInt32(dgvr.Cells[0].Value));
                    dgvRistoranti.Rows.Remove(dgvr);
                }
                UpdateGbxRistoranti();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TRistorante.cn.Close();
            }
        }

        private void btnEliminaCarta_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (DataGridViewRow dgvr in dgvCarteCredito.SelectedRows)
                {
                    TCartaCredito.DeleteCreditCard(Convert.ToInt32(dgvr.Cells[0].Value));
                    TAccetta.DeleteLinkCard(Convert.ToInt32(dgvr.Cells[0].Value));
                    dgvCarteCredito.Rows.Remove(dgvr);
                }
                UpdateGbxCarteDiCredito();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TCartaCredito.cn.Close();
            }
        }

        private void dgvRistoranti_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                TRistorante.UpdateRestaurant(RestaurantByDataGridRow(dgvRistoranti.Rows[e.RowIndex].Cells));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TRistorante.cn.Close();
            }
        }

        private void dgvCarteCredito_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                TCartaCredito.UpdateCreditCard(CreditCardByDataGridRow(dgvCarteCredito.Rows[e.RowIndex].Cells));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TCartaCredito.cn.Close();
            }
        }

        private void btnAccoppia_Click(object sender, EventArgs e)
        {
            try
            {
                TAccetta.Link(Convert.ToInt32(dgvRistoranti.Rows[dgvRistoranti.SelectedCells[0].RowIndex].Cells[0].Value), Convert.ToInt32(dgvCarteCredito.Rows[dgvCarteCredito.SelectedCells[0].RowIndex].Cells[0].Value));
                UpdateGbxRistoranti();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TAccetta.cn.Close();
            }
        }

        private void dgvRistoranti_Click(object sender, EventArgs e)
        {
            try
            {
                lstCarteAss.Items.Clear();
                lstCarteAss.Items.AddRange(TCartaCredito.NameOfCreditCardByIDs(TAccetta.CreditCardIDs(Convert.ToInt32(dgvRistoranti.Rows[dgvRistoranti.SelectedCells[0].RowIndex].Cells[0].Value))).ToArray());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TAccetta.cn.Close();
            }
        }

        private void btnDisaccoppia_Click(object sender, EventArgs e)
        {
            //WORK IN PROGRESS
        }
    }
}