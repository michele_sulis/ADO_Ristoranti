﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO_Ristoranti
{
    class Ristorante
    {
        public int ID_cod { get; set; }
        public string nome { get; set; }
        public string indirizzo { get; set; }
        public int n_civico { get; set; }
        public string citta { get; set; }
        public string provincia { get; set; }
        public int livello { get; set; }

        public Ristorante(string nome, string indirizzo, int n_civico, string citta, string provincia, int livello)
        {
            this.nome = nome;
            this.indirizzo = indirizzo;
            this.n_civico = n_civico;
            this.citta = citta;
            this.provincia = provincia.ToLower();
            this.livello = livello;

            TRistorante.RestaurantWithSameLocation(this);
        }

        public Ristorante(int ID_cod, string nome, string indirizzo, int n_civico, string citta, string provincia, int livello)
        {
            this.ID_cod = ID_cod;
            this.nome = nome;
            this.indirizzo = indirizzo;
            this.n_civico = n_civico;
            this.citta = citta;
            this.provincia = provincia.ToLower();
            this.livello = livello;
        }

    }
}
