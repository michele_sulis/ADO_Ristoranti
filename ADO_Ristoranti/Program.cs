﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ADO_Ristoranti
{
    static class Program
    {
        public static string cnStr = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                "Data Source=" + Directory.GetCurrentDirectory() + @"\Ristoranti.accdb;" +
                @"Persist Security Info=False;";
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmVisualizzaModifica());
        }
    }
}
