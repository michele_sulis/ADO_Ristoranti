﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO_Ristoranti
{
    class CartaCredito
    {
        public int ID_identificativo { get; set; }
        public string nome { get; set; }
        public string num_verde { get; set; }

        public CartaCredito(string nome, string num_verde)
        {
            this.nome = nome;
            int i;
            if (int.TryParse(num_verde, out i))
                this.num_verde = num_verde;
            else
                throw new Exception("Numero verde non valido!");

            TCartaCredito.SameCards(this);
        }

        public CartaCredito(int ID_identificativo, string nome, string num_verde)
        {
            this.ID_identificativo = ID_identificativo;
            this.nome = nome;
            int i;
            if (int.TryParse(num_verde, out i))
                this.num_verde = num_verde;
            else
                throw new Exception("Numero verde non valido!");

            TCartaCredito.SameCards(this);
        }

    }
}
