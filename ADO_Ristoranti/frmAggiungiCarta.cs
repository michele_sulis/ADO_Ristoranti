﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ADO_Ristoranti
{
    public partial class frmAggiungiCarta : Form
    {
        public frmAggiungiCarta()
        {
            InitializeComponent();
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNome.Text) || string.IsNullOrWhiteSpace(txtNVerde.Text))
                btnInserisci.Enabled = false;
            else
                btnInserisci.Enabled = true;
        }

        private void txtNVerde_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtNome.Text) || string.IsNullOrWhiteSpace(txtNVerde.Text))
                btnInserisci.Enabled = false;
            else
                btnInserisci.Enabled = true;
        }

        private void btnInserisci_Click(object sender, EventArgs e)
        {
            try
            {
                CartaCredito c = new CartaCredito(txtNome.Text, txtNVerde.Text);
                TCartaCredito.AddCreditCard(c);
                ControlsAsDefault(txtNome, txtNVerde);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Errore", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void ControlsAsDefault(params TextBox[] tx)
        {
            foreach (var item in tx)
                item.Clear();
        }
    }
}
