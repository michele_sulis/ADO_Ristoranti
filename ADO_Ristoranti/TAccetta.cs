﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO_Ristoranti
{
    static class TAccetta
    {
        public static OleDbConnection cn = new OleDbConnection(Program.cnStr);

        public static string CreateTable()
        {
            string err = "";

            try
            {
                string SQLcmd = "CREATE TABLE Accetta (ID_ris int, ID_carta int, PRIMARY KEY (ID_ris, ID_carta));";
                
                cn.Open();
                OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
                cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                err = e.Message;
                if (e.Message != "Table 'Accetta' already exists.")
                    throw e;
            }

            return err;
        }

        public static int RowCount()
        {
            string cmdString = "SELECT COUNT (*) FROM Accetta";
            
            cn.Open();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = cn;
            cmd.CommandText = cmdString;
            int n = (int)cmd.ExecuteScalar();
            cn.Close();
            return n;
        }

        public static bool ExistingTable()
        {
            try
            {
                RowCount();
            }
            catch (Exception ex)
            {
                if (ex.Message == "The Microsoft Office Access database engine cannot find the input table or query 'Accetta'.  Make sure it exists and that its name is spelled correctly.")
                    return false;
            }
            return true;
        }

        public static void Link(int ID_Ris, int ID_Carta)
        {
            string SQLcmd = "INSERT INTO Accetta (ID_ris, ID_carta) VALUES(@ID_ris, @ID_carta);";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
            cmd.Parameters.AddWithValue("@ID_ris", ID_Ris);
            cmd.Parameters.AddWithValue("@ID_carta", ID_Carta);
            cmd.ExecuteNonQuery();
            cn.Close();
        }

        public static List<int> CreditCardIDs(int ID_ris)
        {
            List<int> IDs = new List<int>();
            string SQLcmd = "SELECT * FROM Accetta WHERE ID_ris = @ID_ris;";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
            cmd.Parameters.AddWithValue("@ID_ris", ID_ris);
            OleDbDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
                IDs.Add(Convert.ToInt32(dr["ID_carta"]));
            cn.Close();

            return IDs;
        }

        public static void DeleteLinkCard(int ID_carta)
        {
            string SQLcmd = "DELETE FROM Accetta WHERE ID_carta = @ID_carta;";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
            cmd.Parameters.AddWithValue("@ID_carta", ID_carta);
            cmd.ExecuteNonQuery();
            
            cn.Close();
        }

        public static void DeleteLinkRestaurant(int ID_ris)
        {
            string SQLcmd = "DELETE FROM Accetta WHERE ID_ris = @ID_ris;";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
            cmd.Parameters.AddWithValue("@ID_ris", ID_ris);
            cmd.ExecuteNonQuery();

            cn.Close();
        }

        public static void DeleteFullLink(int ID_carta, int ID_ris)
        {
            string SQLcmd = "DELETE FROM Accetta WHERE ID_ris = @ID_ris AND ID_carta = @ID_carta;";

            cn.Open();
            OleDbCommand cmd = new OleDbCommand(SQLcmd, cn);
            cmd.Parameters.AddWithValue("@ID_ris", ID_ris);
            cmd.Parameters.AddWithValue("@ID_carta", ID_carta);
            cmd.ExecuteNonQuery();
            cn.Close();
        }
    }
}